import torch
import kornia
from utils.plot import plot_six_images


class SpatialPhotometricConsistencyLoss(torch.nn.Module):
    def __init__(self, baseline, focal_length, left_camera_matrix, right_camera_matrix, trans_matrix, lambda_s):
        super().__init__()
        self.BF = baseline * focal_length
        self.left_camera_matrix = left_camera_matrix
        self.right_camera_matrix = right_camera_matrix
        self.transformation_matrix = trans_matrix
        self.lambda_s = lambda_s
        self.L1Loss = torch.nn.L1Loss()
        self.SSIMLoss = kornia.losses.SSIMLoss(window_size=11)

    def synthesize_images(self, left_image, right_image, left_current_depth, right_current_depth):
        synthesized_right_image = kornia.geometry.warp_frame_depth(
            image_src=left_image,
            depth_dst=right_current_depth,
            src_trans_dst=self.transformation_matrix,
            camera_matrix=self.left_camera_matrix,
            normalize_points=True
        )

        synthesized_left_image = kornia.geometry.warp_frame_depth(
            image_src=right_image,
            depth_dst=left_current_depth,
            src_trans_dst=torch.inverse(self.transformation_matrix),
            camera_matrix=self.right_camera_matrix,
            normalize_points=True
        )

        plot_six_images(
            left_image, right_current_depth, synthesized_right_image,
            right_image, left_current_depth, synthesized_left_image,
            fig_name='SpatialPhotometricConsistencyLoss'
        )

        return synthesized_left_image, synthesized_right_image

    def forward(self, left_image, right_image, left_depth, right_depth):
        synthesized_left_image, synthesized_right_image = self.synthesize_images(
            left_image,
            right_image,
            left_depth,
            right_depth
        )

        right_image_loss = (1 - self.lambda_s) * self.L1Loss(right_image, synthesized_right_image) + \
                           self.lambda_s * self.SSIMLoss(right_image, synthesized_right_image)
        left_image_loss = (1 - self.lambda_s) * self.L1Loss(left_image, synthesized_left_image) + \
                          self.lambda_s * self.SSIMLoss(left_image, synthesized_left_image)

        return right_image_loss + left_image_loss
