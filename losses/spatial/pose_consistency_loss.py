import torch


class SpatialPoseConsistencyLoss(torch.nn.Module):
    def __init__(self, l_rotation, l_translation):
        super().__init__()
        self.l_rotation = l_rotation
        self.l_translation = l_translation
        self.L1Loss = torch.nn.L1Loss()

    def forward(self, left_translation, left_rotation, right_translation, right_rotation):
        rotation_loss = self.l_rotation * self.L1Loss(left_rotation, right_rotation)
        translation_loss = self.l_translation * self.L1Loss(left_translation, right_translation)

        return rotation_loss + translation_loss
