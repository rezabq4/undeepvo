import torch
import kornia
from utils.loss import construct_transformation_matrix
from utils.plot import plot_six_images


class TemporalGeometricConsistencyLoss(torch.nn.Module):
    def __init__(self, camera_matrix):
        super().__init__()
        self.camera_matrix = camera_matrix
        self.L1Loss = torch.nn.L1Loss()

    def synthesize_next_depth(self, current_depth, next_depth, next_to_current_transformation):
        synthesized_next_image = kornia.geometry.warp_frame_depth(
            image_src=current_depth,
            depth_dst=next_depth,
            src_trans_dst=next_to_current_transformation,
            camera_matrix=self.camera_matrix,
            normalize_points=True
        )
        return synthesized_next_image

    def synthesize_current_depth(self, next_depth, current_depth, current_to_next_transformation):
        synthesized_current_image = kornia.geometry.warp_frame_depth(
            image_src=next_depth,
            depth_dst=current_depth,
            src_trans_dst=current_to_next_transformation,
            camera_matrix=self.camera_matrix,
            normalize_points=True
        )
        return synthesized_current_image

    def forward(self, current_depth, next_depth, current_position, current_angle, next_position, next_angle):
        next_to_current_transformation = construct_transformation_matrix(current_position, current_angle)
        current_to_next_transformation = construct_transformation_matrix(next_position, next_angle)

        synthesized_next_depth = self.synthesize_next_depth(current_depth, next_depth,
                                                            next_to_current_transformation)
        synthesized_current_depth = self.synthesize_current_depth(next_depth, current_depth,
                                                                  current_to_next_transformation)

        plot_six_images(
            current_depth, next_depth, synthesized_next_depth,
            next_depth, current_depth, synthesized_current_depth,
            fig_name="TemporalGeometricConsistencyLoss"
        )

        current_loss = self.L1Loss(current_depth, synthesized_current_depth)

        next_loss = self.L1Loss(next_depth, synthesized_next_depth)

        return current_loss + next_loss
