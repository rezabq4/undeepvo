import os

import pandas as pd
import torch
from torch.utils.data import DataLoader
from torchsummary import summary

from depth_estimator.unet.model import UNet
from utils.data_generator import DiodeLoader

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def download_dataset(url: str = "http://diode-dataset.s3.amazonaws.com/val.tar.gz"):
    """
    :param url: url to the dataset file
    """
    annotation_folder = "/dataset/"
    if not os.path.exists(os.path.abspath("..") + annotation_folder):
        annotation_zip = tf.keras.utils.get_file(
            "val.tar.gz",
            cache_subdir=os.path.abspath(".."),
            origin=url,
            extract=True
        )


def prepare_dataset(directory: str):
    """
    :param directory: dir_url to dataset
    """
    path = os.path.join('val', directory)
    file_list = []

    for root, dirs, files in os.walk(path):
        for file in files:
            file_list.append(os.path.join(root, file))

    file_list.sort()

    dataset = {
        "image": [x for x in file_list if x.endswith(".png")],
        "depth": [x for x in file_list if x.endswith("_depth.npy")],
        "mask": [x for x in file_list if x.endswith("_depth_mask.npy")],
    }

    df = pd.DataFrame(dataset)
    df = df.sample(frac=1, random_state=42)

    return df


class TrainPipeline:
    def __init__(self):
        self.CLASSES = 1
        self.N_CHANNELS = 3
        self.BILINEAR = False
        self.HEIGHT = 256
        self.WIDTH = 256
        self.LR = 0.0002
        self.EPOCHS = 30
        self.BATCH_SIZE = 16
        # Set up model

        self.model = UNet(n_channels=self.N_CHANNELS, n_classes=self.CLASSES, bilinear=self.BILINEAR).to(device)
        self.model = self.model.to(device)

        self.loss_function = torch.nn.MSELoss().to(device)
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.LR)
        self.dataset = DiodeLoader(data=df, batch_size=self.BATCH_SIZE, dim=(self.HEIGHT, self.WIDTH),
                                   n_channels=self.N_CHANNELS, shuffle=True)
        self.loader = DataLoader(self.dataset, batch_size=self.BATCH_SIZE, shuffle=True)

    def model_summary(self):
        summary(self.model, input_size=(3, 256, 256))

    def get_dataset(self):
        return self.dataset

    def get_loader(self):
        return self.loader

    def train(self):
        losses = []
        val_losses = []
        best_loss = float('Inf')
        best_val_loss = float('Inf')

        running_val_loss = 0.0
        self.model.eval()
        for e in range(self.EPOCHS):
            for batch in self.loader:
                x, y = batch

                x = x.to(device)
                y = y.to(device)

                # One optimization iteration
                self.optimizer.zero_grad()
                y_pred = self.model(x)
                loss = self.loss_function(y_pred, y)
                loss.backward()
                self.optimizer.step()
                losses.append(loss.item())
                print(f"############## EPOCH {e}'th ##############")
                print(f"############## LOSS  {loss.item()} ##############")


if __name__ == '__main__':
    dataset_type = "outdoor"
    df = prepare_dataset(directory=dataset_type)

    train_pipeline = TrainPipeline()
    train_pipeline.model_summary()
    train_pipeline.train()
