import os

import mlflow
import mlflow.exceptions

DEFAULT_EXPERIMENT_NAME = "UnDeepVO"
HOST_URI = "file:///home/mohammadreza/Project/UnDeepVO/logs"
CREATE_DATABRICKS_CREDENTIALS = False


class MlFlowHandler(object):
    def __init__(
            self,
            experiment_name=DEFAULT_EXPERIMENT_NAME,
            mlflow_tags={},
            mlflow_parameters={}
    ):
        mlflow.set_tracking_uri(HOST_URI)
        self._experiment_name = experiment_name
        self._mlflow_client = mlflow.tracking.MlflowClient(HOST_URI)
        self._enable_mlflow = True
        self._mlflow_tags = mlflow_tags
        self._mlflow_parameters = mlflow_parameters

    def start_callback(self, parameters):
        try:
            mlflow.set_experiment(self._experiment_name)
            if mlflow.active_run() is not None:
                mlflow.end_run()
            mlflow.start_run()
            mlflow.set_tags(self._mlflow_tags)
            mlflow.log_params(parameters)
            mlflow.log_params(self._mlflow_parameters)

        except mlflow.exceptions.MlflowException as msg:
            self._enable_mlflow = False
            print(f"[WARNING][MlFlowHandler] - [StartCallback] {msg}")
            print(f"[WARNING][MlFlowHandler] - [StartCallback] mlflow is disabled")

    def finish_callback(self):
        if not self._enable_mlflow:
            return
        try:
            mlflow.end_run()
        except mlflow.exceptions.MlflowException as msg:
            self._enable_mlflow = False
            print(f"[WARNING][MlFlowHandler] - [FinishCallback] {msg}")
            print(f"[WARNING][MlFlowHandler] - [FinishCallback] mlflow is disabled")

    def epoch_callback(self, metrics, current_epoch=0, artifacts=None):
        if not self._enable_mlflow:
            return
        try:
            metrics["epoch"] = current_epoch
            mlflow.log_metrics(metrics, current_epoch)
            if artifacts is not None:
                for artifact in artifacts:
                    mlflow.log_artifact(artifact)
                    os.remove(artifact)
        except mlflow.exceptions.MlflowException as msg:
            self._enable_mlflow = False
            print(f"[WARNING][MlFlowHandler] - [EpochCallback] {msg}")
            print(f"[WARNING][MlFlowHandler] - [EpochCallback] mlflow is disabled")
