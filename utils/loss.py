import torch


def euler_to_rotation(angle: torch.tensor):
    batch_size = angle.shape[0]

    zeros = torch.zeros(batch_size, 1)
    ones = torch.ones(batch_size, 1)

    x, y, z = angle[:, 0], angle[:, 1], angle[:, 2]

    x_cos = torch.cos(x)
    x_sin = torch.sin(x)

    x = torch.tensor([
        [
            x_cos, x_sin, zeros,
            -x_sin, x_cos, zeros,
            zeros, zeros, ones
        ]
    ]).reshape(batch_size, 3, 3)

    y_cos = torch.cos(y)
    y_sin = torch.sin(y)

    y = torch.tensor([
        [
            y_cos, zeros, -y_sin,
            zeros, ones, zeros,
            y_sin, zeros, y_cos
        ]
    ]).reshape(batch_size, 3, 3)

    z_cos = torch.cos(z)
    z_sin = torch.sin(z)

    z = torch.tensor([
        [
            z_cos, z_sin, zeros,
            -z_sin, z_cos, zeros,
            zeros, zeros, ones
        ],
    ]).reshape(batch_size, 3, 3)

    return torch.bmm(torch.bmm(x, y), z)


def construct_transformation_matrix(angle, translation_matrix):
    """
        Construct transformation matrix given angle matrix(B, 3) and translation matrix(B, 3)
        angle: Bx3
        translation_matrix: Bx3
        transformation matrix : Bx4x4
    """
    batch_size = angle.shape[0]
    assert angle.shape[0] == translation_matrix.shape[0]
    rotation_matrix = euler_to_rotation(angle)  # Bx3x3
    rotation_matrix = rotation_matrix.to(translation_matrix.device)
    assert rotation_matrix.shape[0] == translation_matrix.shape[0]

    translation_matrix = translation_matrix.unsqueeze(dim=2)  # Bx3x1
    transformation_matrix = torch.cat((rotation_matrix, translation_matrix), dim=2)  # Bx3x4
    transformation_matrix = transformation_matrix.to(translation_matrix.device)

    identity = torch.tensor([0, 0, 0, 1]).to(translation_matrix.device)  # 4
    identity = identity.repeat(batch_size, 1, 1)  # Bx1x4
    identity = identity.to(translation_matrix.device)

    transformation_matrix = torch.cat((transformation_matrix, identity), dim=1)  # Bx4x4
    transformation_matrix = transformation_matrix.to(translation_matrix.device)

    return transformation_matrix


def construct_relative_transformation(rotation, translation, next_rotation, next_translation):
    """
        Construct T_k_k+1 given rotation and translation of I_k frame and
        rotation and translation of I_k+1
    """
    I_k1_transformation = construct_transformation_matrix(rotation, translation)
    I_k2_transformation = construct_transformation_matrix(next_rotation, next_translation)

    return kornia.geometry.relative_transformation(I_k1_transformation, I_k2_transformation)
