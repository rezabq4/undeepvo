import torch
from easydict import EasyDict as edict

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print('Using device:', device)


class UnetParams(object):
    HEIGHT = 125
    WIDTH = 414
    BATCH_SIZE = 1
    N_CHANNELS = 3
    EPOCHS = 32
    N_CLASSES = 1


class PoseNetParams:
    HEIGHT = 125
    WIDTH = 414
    N_CHANNEL = 3
    EPOCHS = 30
    BATCH_SIZE = 1


class CameraParams:
    SCALE = 3
    CX = 6.071928000000e+02
    CY = 1.852157000000e+02
    BASELINE = 0.46 / 3
    FOCAL_LENGTH = 7.188560000000e+02
    HEIGHT = 375
    WIDTH = 1242

    SCALED_WIDTH = WIDTH / SCALE
    SCALED_HEIGHT = HEIGHT / SCALE

    SCALED_CX = SCALED_WIDTH / 2 + (CX - WIDTH / 2) / SCALE
    SCALED_CY = SCALED_HEIGHT / 2 + (CY - HEIGHT / 2) / SCALE
    SCALED_FOCAL_LENGTH = FOCAL_LENGTH / SCALE

    RIGHT_CAMERA_MATRIX = torch.tensor([
        [SCALED_FOCAL_LENGTH, 0, SCALED_CX],
        [0, SCALED_FOCAL_LENGTH, SCALED_CY],
        [0, 0, 1]
    ]).to(device)[None].float()

    LEFT_CAMERA_MATRIX = torch.tensor([
        [SCALED_FOCAL_LENGTH, 0, SCALED_CX],
        [0, SCALED_FOCAL_LENGTH, SCALED_CY],
        [0, 0, 1]
    ]).to(device)[None].float()

    # RIGHT_CAMERA_MATRIX = torch.tensor([
    #     [7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02],
    #     [0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02],
    #     [0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00]
    # ])[None].to(device).float()
    #
    # LEFT_CAMERA_MATRIX = torch.tensor([
    #     [7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02],
    #     [0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02],
    #     [0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00]
    # ])[None].to(device).float()
    DEFAULT_TRANSFORMATION_MATRIX = torch.tensor(
        [
            [1, 0, 0, 5],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1],
        ]
    ).to(device)[None].float()


class TrainConstants:
    EPOCHS = 32
    LR = 1e-4
    SPATIAL_LAMBDA_S = 0.9
    SPATIAL_LAMBDA_ROTATION = 0.7
    SPATIAL_LAMBDA_TRANSLATION = 0.3
    TEMPORAL_LAMBDA_S = 0.9
    MIN_DEPTH = 1
    MAX_DEPTH = 100


dict_parameters = edict({
    'data_dir': './dataset/',
    'input_height': UnetParams.HEIGHT,
    'input_width': UnetParams.WIDTH,
    'mode': 'train',
    'epochs': UnetParams.EPOCHS,
    'batch_size': UnetParams.BATCH_SIZE,
    'do_augmentation': True,
    'augment_parameters': [0.8, 1.2, 0.5, 2.0, 0.8, 1.2],
    'input_channels': UnetParams.N_CHANNELS,
    'num_workers': 2,
})
